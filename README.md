# Kubernetes Lab in Azure

A terraform module to create an Azure Kubernetes Service (AKS) managed cluster. 

## Usage Example

Cluster autoscaling is enabled, but can be effectively disabled by setting min and max node counts to the same number.

```hcl
module "az_kube_deploy" {
  source = "git::https://gitlab.com/jarrett-ccg/kubernetes-lab-in-azure.git"

  location = "East US"
  prefix   = "my-cluster"

  vm_size            = "Standard_B2s"
  min_node_count     = 1     # default
  max_node_count     = 3     # default
  rbac_enabled       = true  # default
  deploy_registry    = false # default
}
```

## Output Example

Outputs from this module will need to be passed through to be made useful.

The following example passes kube_config through:

```hcl
output "kube_config" {
  value = module.az_kube_deploy.kube_config
}
output "registry_url" {
  value = module.az_kube_deploy.registry_url
}
```
