output "id" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.id}"
  description = "The ID of the Kubernetes Managed Cluster."
}

output "client_key" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.kube_config.0.client_key}"
  description = "Base64 encoded private key used by clients to authenticate to the Kubernetes cluster."
}

output "client_certificate" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.kube_config.0.client_certificate}"
  description = "Base64 encoded public certificate used by clients to authenticate to the Kubernetes cluster."
}

output "cluster_ca_certificate" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.kube_config.0.cluster_ca_certificate}"
  description = "Base64 encoded public CA certificate used as the root of trust for the Kubernetes cluster."
}

output "host" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.kube_config.0.host}"
  description = "The Kubernetes cluster server host."
}

output "kube_config" {
  value       = "${azurerm_kubernetes_cluster.k8s_cluster.kube_config_raw}"
  description = "Fully formed kube_config ready to use with kubectl."
}

output "registry_url" {
  value = azurerm_container_registry.registry.*.login_server
}
