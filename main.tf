provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  location = var.location
  name     = "${var.prefix}_k8s_rg"
}

resource "azurerm_kubernetes_cluster" "k8s_cluster" {
  name                              = "${var.prefix}_k8s_cluster"
  location                          = var.location
  resource_group_name               = azurerm_resource_group.rg.name
  dns_prefix                        = var.prefix
  role_based_access_control_enabled = var.rbac_enabled

  default_node_pool {
    name                   = "default"
    enable_auto_scaling    = true
    min_count              = var.min_node_count
    max_count              = var.max_node_count
    vm_size                = var.vm_size
    enable_host_encryption = true
    vnet_subnet_id         = azurerm_subnet.internal.id
    pod_subnet_id          = azurerm_subnet.pod.id

  }

  network_profile {
    network_plugin     = "azure"
    service_cidr       = "172.32.255.0/24"
    dns_service_ip     = "172.32.255.2"
    docker_bridge_cidr = "192.168.255.0/24"
  }

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_container_registry" "registry" {
  count = var.deploy_registry ? 1 : 0

  name                = var.prefix
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Basic"
}
