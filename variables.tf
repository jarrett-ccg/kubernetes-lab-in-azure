variable "location" {
  description = "The Azure Region in which all resources in this example should be provisioned"
}

variable "prefix" {
  description = "dns prefix for cluster resources"
}

variable "min_node_count" {
  description = "minimum number of nodes in the pool"
  type        = number
  default     = 1
}

variable "max_node_count" {
  description = "maximum number of nodes in the pool"
  type        = number
  default     = 3
}

variable "vm_size" {
  description = "Azure Virtual Machine size for nodes in pool"
}

variable "rbac_enabled" {
  description = "Enable RBAC in the cluster"
  type        = bool
  default     = true
}

variable "deploy_registry" {
  description = "Deploy a container registry alongside the cluster"
  type        = bool
  default     = false
}
